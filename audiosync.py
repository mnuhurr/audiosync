"""
AudioSync

Time synchronize audio recordings using landmark-based fingerprints, sync classes

2019-09-05 Manu Harju, manu.harju@tuni.fi

"""

# python version compatibility
from __future__ import division, print_function


# This is an ugly hack to get the imports in audfprint to work. Another way is to modify the lines in audfprint folder,
# e.g. import audio_read -> from . import audio_read
import sys
sys.path.insert(1, 'audfprint')

import bisect
import statistics
from operator import itemgetter

#import numpy as np

# for the sake of clarity use full path, though now these could be found with the filename only
from audfprint.audfprint_analyze import Analyzer, landmarks2hashes


def counts(x):
    uniq_vals = list(set(x))
    cnt_dict = {v: 0 for v in uniq_vals}

    for i in x:
        cnt_dict[i] += 1

    return cnt_dict

class OffsetFinder:
    """
    A simple class to find the time offsets of audio files to a given reference file

    """

    def __init__(self, reference, analyzer=None):
        """
        constructor: reference can be either a filename or a list of landmarks
        """

        if len(reference) > 0 and len(reference[0]) > 1:
            # landmarks
            self.landmarks = reference.copy()

        else:
            # file
            if analyzer is None:
                analyzer = Analyzer()

            peaks = analyzer.wavfile2peaks(reference, analyzer.shifts)
            self.landmarks = analyzer.peaks2landmarks(peaks)
            self.hashes = landmarks2hashes(self.landmarks).tolist()

            # sort by hash
            self.hashes.sort(key=itemgetter(1))



    def _hashmatch(self, hashes, stop_after=500):
        """
        find matching hashes, but drop out suspicious looking ones, currently:
         - if the same hash is matched more than two times, all of its matches are leaved out

        :param hashes: list of audfprint format hashes
        :param stop_after: after how many matches we stop the search
        :return: list of time offsets

        """
        offsets = []

        if self.hashes is None:
            self.hashes = landmarks2hashes(self.landmarks)

        hashes.sort(key=itemgetter(1))
        only_hashes = list(map(itemgetter(1), hashes))

        start_ind = 0

        prev_ref = -1

        # count the number of found offsets, and stop after the given number of hits
        count = 0

        for ref_hash in self.hashes:
            # is it time to find a new start index (did we start or did the hash change)
            if ref_hash[1] != prev_ref:
                prev_ref = ref_hash[1]
                start_ind = bisect.bisect_left(only_hashes, ref_hash[1])

            if start_ind >= len(hashes):
                break

            if stop_after > 0 and count >= stop_after:
                break

            if ref_hash[1] == hashes[start_ind][1]:
                # go through every matching hash
                end_ind = bisect.bisect_right(only_hashes, ref_hash[1])

                for hash_ in hashes[start_ind:end_ind]:
                    offset = ref_hash[0] - hash_[0]

                    offsets.append(offset)
                    count += 1

                    #if ref_hash[1] not in matches:
                    #    matches[ref_hash[1]] = [offset]
                    #else:
                    #    matches[ref_hash[1]].append(offset)



        return offsets



    def syncfile(self, filename, analyzer=None):
        """
        sync files to the reference audio landmarks

        :param filename: filename of the wav
        :param analyzer: an Analyzer object to construct landmarks
        :type filename: str
        :type analyzer: Analyzer
        :return:
        :rtype: ??
        """

        if analyzer is None:
            analyzer = Analyzer()

        peaks = analyzer.wavfile2peaks(filename, shifts=analyzer.shifts)
        landmarks = analyzer.peaks2landmarks(peaks)

        hashes = landmarks2hashes(landmarks).tolist()
        offsets = self._hashmatch(hashes, stop_after=0)

        # didn't get any?
        if len(offsets) == 0:
            return None

        # todo: check how clearly we got it?
        #hist, bin_edges = np.histogram(offsets)
        #print(hist, bin_edges)

        try:
            md = statistics.mode(offsets)
        except:
            # can't find the mode. try some guessing
            return None

        # time_unit = n_hop / sample_rate
        tu = analyzer.n_hop / analyzer.target_sr

        return md * tu


class AudioSync:
    """
    AudioSync
    main class to wrap up everything
    """

    def __init__(self, samplerate=11025, density=20, maxpeaksperframe=5, shifts=0, reference_density_diff=0, density_inc=10, max_density=2000):
        """
        constructor: samplerate, density, maxpeaksperframe and shifts are default values for the analyzer used.
        for the reference use density density + reference_density_diff. (to use e.g. higher density for the reference audio)
        density_inc is the increase of density in case when no offset is found with current parameters.
        """
        # store parameters
        self.samplerate = samplerate
        self.density = density
        self.maxpeaksperframe = maxpeaksperframe
        self.shifts = shifts
        self.reference_density_diff = reference_density_diff
        self.density_inc = density_inc
        self.max_density = max_density

    @staticmethod
    def _setup_analyzer(samplerate, density, maxpeaksperframe, shifts):

        """
        static method to setup a new analyzer object in the usual manner
        """

        analyzer = Analyzer()

        analyzer.density = density
        analyzer.maxpksperframe = maxpeaksperframe
        analyzer.f_sd = 30.0
        analyzer.shifts = shifts

        analyzer.target_sr = samplerate
        analyzer.n_fft = 512
        analyzer.n_hop = analyzer.n_fft // 2

        return analyzer


    def sync_files(self, filenames):
        """
        the main thing. if self.density_inc > 0, try until an offset is found by increasing the density of the
        landmarks generated from the inspected file. the density of the reference landmarks stay the same.

        returns a list of the same length as filenames, offsets in seconds
        """
        n_files = len(filenames)

        if n_files < 2:
            return [0.0] * n_files

        # default analyzer to use most of the time. however, sometimes this may fail
        ref_density = self.density + self.reference_density_diff

        analyzer = self._setup_analyzer(self.samplerate, ref_density, self.maxpeaksperframe, self.shifts)

        # use the first one as the reference
        of = OffsetFinder(reference=filenames[0], analyzer=analyzer)

        # set the analyzer density for the other files
        analyzer.density = self.density

        offsets = [0.0]

        for fn in filenames[1:]:
            offset = of.syncfile(fn, analyzer)

            dens = self.density

            if self.density_inc > 0:
                # didn't get it on the first time. should we continue trying:
                while offset is None:
                    dens += self.density_inc
                    analyzer_ = self._setup_analyzer(self.samplerate, dens, self.maxpeaksperframe, self.shifts)
                    offset = of.syncfile(fn, analyzer_)

                    # don't get stuck in the loop
                    if dens >= self.max_density:
                        break


            offsets.append(offset)


        return offsets
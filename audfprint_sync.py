"""
audfprint_sync.py


Use audfprint to sync audio files (i.e. to find the time offsets)

2019-09-11 Manu Harju

"""


# python version compatibility
from __future__ import division, print_function

# This is an ugly hack to get the imports in audfprint to work. Another way is to modify the lines in audfprint folder,
# e.g. import audio_read -> from . import audio_read
import sys
sys.path.insert(1, 'audfprint')

from audfprint.audfprint_analyze import Analyzer
from audfprint.audfprint_match import Matcher
from audfprint.hash_table import HashTable

import time

def main(argv):
    # we can now assume len(fn) >= 2
    filenames = argv[1:]

    # construct the toolset
    analyzer = Analyzer()
    matcher = Matcher()
    ht = HashTable()

    # time unit
    tu = analyzer.n_hop / analyzer.target_sr

    t0 = time.time()

    # use the first one as the reference
    analyzer.ingest(ht, filenames[0])
    matches = [[filenames[0], 0.0]]

    # ...and match the rest
    for fn in filenames[1:]:
        hashes = analyzer.wavfile2hashes(fn)
        res = matcher.match_hashes(ht, hashes)

        # did it match
        if len(res) > 0:
            time_offset = tu * res[0][2]
            matches.append([fn, time_offset])

    # print out the results
    for filename, offset in matches:
        print(filename, offset)

    print('syncing done in %.2f seconds' % (time.time() - t0))

if __name__ == '__main__':
    if len(sys.argv) > 2:
        main(sys.argv)
    else:
        print('nothing to synchronize')
# audiosync
Time synchronize audio recordings using landmark-based fingerprints

### files
- sync.py: command line interface for the tool
- audfprint_sync.py command line interface to use the methods available in [audfprint](https://github.com/dpwe/audfprint) to the job
- audiosync.py classes for audio syncing

[audfprint](https://github.com/dpwe/audfprint) is used as a submodule, and can
be fetched with `git submodule update --init`

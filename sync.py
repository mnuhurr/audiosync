"""
AudioSync

Time synchronize audio recordings using landmark-based fingerprints, command line interface

2019-09-05 Manu Harju, manu.harju@tuni.fi

"""

# python version compatibility
from __future__ import division, print_function

# This is an ugly hack to get the audfprint imports to work. Another way is to modify the lines in audfprint folder,
# e.g. import audio_read -> from . import audio_read
import sys
sys.path.insert(1, 'audfprint')

import time
import argparse
import os

# more version compatibility things
if sys.version_info[0] >= 3:
    time_clock = time.process_time
else:
    time_clock = time.clock

# local imports
from audiosync import AudioSync


def main(args):
    # build the list of files used. the first one is the reference
    fns = [args.ref_file[0]] + args.file

    t0 = time_clock()

    # construct the synchronizer and do the trick
    audsync = AudioSync(samplerate=args.samplerate, density=args.density, maxpeaksperframe=args.max_peaks_per_frame,
                        shifts=args.shifts, reference_density_diff=args.ref_diff, density_inc=args.dens_inc)

    offsets = audsync.sync_files(fns)

    dt = time_clock() - t0

    for fn, offset in zip(fns, offsets):
        print(os.path.basename(fn), offset)

    print('time elapsed: %.1fs' % dt)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--density', dest='density', type=float, help='Target hashes per second (default 40.0)', default=40.0)
    parser.add_argument('-c', '--density-inc', dest='dens_inc', type=float, help='Increase of density if no offset is found (default: 10.0)', default=10.0)
    parser.add_argument('-r', '--ref-density-diff', dest='ref_diff', type=float, help='Increase of density for the reference audio (default: 0.0)', default=0.0)
    parser.add_argument('-P', '--pks-per-frame', dest='max_peaks_per_frame', type=int, help='Maximum number of peaks per frame (default: 6)', default=6)
    parser.add_argument('-s', '--shifts', dest='shifts', type=int, help='Use this many subframe shifts building fp (default: 0)', default=0)
    parser.add_argument('-F', '--samplerate', dest='samplerate', type=int, help='Resample input files to this (default: 8000)', default=8000)
    parser.add_argument('ref_file', nargs=1, type=str, help='the reference wav file')
    parser.add_argument('file', nargs='+', type=str, help='wav file(s) to sync')

    args = parser.parse_args()
    main(args)